﻿Function Get-Folder($initialDirectory) {
    [void] [System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms')
    $FolderBrowserDialog = New-Object System.Windows.Forms.FolderBrowserDialog
    $FolderBrowserDialog.RootFolder = 'MyComputer'
    $folderBrowserDialog.Description = "Choose the Everquest folder"
    if ($initialDirectory) { $FolderBrowserDialog.SelectedPath = $initialDirectory }
    $result = $FolderBrowserDialog.ShowDialog((New-Object System.Windows.Forms.Form -Property @{TopMost = $True }))
    if ($result -eq  [System.Windows.Forms.DialogResult]::Ok) {
        return $FolderBrowserDialog.SelectedPath
    } else {
        Write-Information -InformationAction Stop "No folder selected."
    }
}

[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')


$eq_folder = Get-Folder("C:\Users\Public\Daybreak Game Company\Installed Games\Everquest")

# validate folder - look for LaunchPad.exe
if (! (Test-Path -Path ($eq_folder + "\LaunchPad.exe"))) {
    Write-Information -InformationAction Stop "Selected folder doesn't appear to be an Everquest folder: $eq_folder"
}


$character = [Microsoft.VisualBasic.Interaction]::InputBox("Character name:", "EQ Link", "")
if (! $character) {
    Write-Information -InformationAction Stop "Character name not provided"
}

$new_folder = ($eq_folder + "\..\EverQuest " + $character.Trim())

# Make sure the eqclient.ini file exists already
$ini_file = $eq_folder + "\" + $character.Trim() + "_eqclient.ini"

if (! (Test-Path -Path $ini_file)) {
    Write-Information -InformationAction Stop "Selected folder doesn't contain a character-specific ini file named: $ini_file"
}


if (Test-Path -Path $new_folder) {
    Write-Information -InformationAction Stop "Folder already exists: $new_folder"
}

Write-Information -InformationAction Continue "Creating folder: $new_folder"
[void](New-Item -Path $new_folder -ItemType Directory)

Write-Information -InformationAction Continue "Creating links..."
Get-ChildItem -Path $eq_folder |
    ForEach-Object {
        $relative_name = $_.FullName.replace($eq_folder, "")
        # create symbolic link to the file
        if (! ($relative_name -match "^\.git") -and ! ($_.Name -eq "eqclient.ini") ) {
            [void](New-Item -Path ($new_folder + $relative_name) -Value $_.FullName -ItemType SymbolicLink)
        }
    }


[void](New-Item -ItemType SymbolicLink -Path ($new_folder + "\eqclient.ini") -Value $ini_file)

$launchpad_link = $HOME + "\Desktop\Everquest " + $character
if (! (Test-Path -Path $launchpad_link)) {
    Write-Information -InformationAction Continue "Creating symbolic link to LaunchPad.exe on Desktop"
    [void](New-Item -ItemType SymbolicLink -Path $launchpad_link -Value ($new_folder + "\LaunchPad.exe"))
}

Write-Information -InformationAction Continue "All links created."